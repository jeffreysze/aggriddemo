import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'agGridDemo';

  columnDefs;
  // = [
    // {headerName: 'Make', field: 'make', sortable: true, filter: true, checkboxSelection: true},
    // {headerName: 'Model', field: 'model', sortable: false, filter: true},
    // {headerName: 'Price', field: 'price', sortable: true, filter: true}
  // ];

  rowData;
  //  = [];

  constructor(private dataService: DataService, private http: HttpClient) {

  }

  ngOnInit(): void {
    this.dataService.getData().subscribe(data => {
      console.log(data);
      let tempCol = [];
      tempCol.push({headerName: 'Make', field: 'make', sortable: true, filter: true, checkboxSelection: true});
      tempCol.push({headerName: 'Model', field: 'model', sortable: false, filter: true});
      tempCol.push({headerName: 'Price', field: 'price', sortable: true, filter: true});
      this.columnDefs = tempCol;
  
      let tempData = [];
      tempData.push({ make: 'Toyota', model: 'Celica', price: 35000 });
      tempData.push({ make: 'Ford', model: 'Mondeo', price: 32000 });
      tempData.push({ make: 'Porsche', model: 'Boxter', price: 72000 });
  
      this.rowData = tempData;
    })


  }
  
}
